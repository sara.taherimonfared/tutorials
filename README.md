This repository contains mini-tutorials for PYTHIA (see https://www.pythia.org) for special/advanced topics. Tutorials have usually been given at summer schools or smaller meetings, and the material is kept here for posterity.

The tutorials assumes some familiarity with PYTHIA, at the level of having been through the PYTHIA worksheet, or what is usually given as a first day tutorial at a summer school. The tutorials are provided as slides, with the possibility of having some auxillary files attached, which needs to be available. We attempt to keep the tutorials reasonably up to date, but the repository is meant mostly as a source for self-study.

If you would like to have PYTHIA tutorials at your summer school, please contact us at authors@pythia.org, and we might be able to present one in person.
