# Tuning Tutorial

Contact: Phil Ilten

## Getting started
You can download the Docker container for the tutorial in advance using the following command.
```
  docker pull pythia8/tutorials:hsf23
```
To start up the tutorial, run the following.
```
./run.sh
```
This will start a Jupyter notebook server, and create the folder `tutorial`. Please follow the instructions in `tutorial/tuning.ipynb`. This worksheet is based on Pythia's Python interface. For a more introdctory tutorial into Pythia, first look at `tutorial/worksheet8310.ipynb`.

## There's more
If you'd like to work with C++, you can either follow the instructions in `tutorial/pdfdoc/worksheet8309.pdf` to install Pythia locally (note, this worksheet was written specifically for Pythia 8.309, but is applicable also for 8.310), or run the container interactively with the following.
```
./run.sh bash
```
This will start a `bash` shell within the Docker container, where Pythia is already installed, and the examples can be compiled.
