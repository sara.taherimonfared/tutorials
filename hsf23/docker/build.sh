#!/bin/bash

# Grab the Pythia source and build the full Python interface.
if [ ! -f pythia8310.tgz ]; then
    wget https://pythia.org/download/pythia83/pythia8310.tgz
    tar -xzvf pythia8310.tgz && rm pythia8310.tgz
    cp generate.sh pythia8310/plugins/python/generate
    cd pythia8310/plugins/python
    ./generate --full
    cd - && tar -czvf pythia8310.tgz pythia8310
    rm -rf pythia8310
fi

# Build the Docker container.
docker build --network=host -t pythia8/tutorials:hsf23 .
