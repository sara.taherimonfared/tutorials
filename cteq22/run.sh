#!/usr/bin/env bash
docker run --rm -u `id -u $USER` -v $PWD:$PWD -w $PWD -p 8888:8888 -it pythia8/tutorials:cteq22 $@
