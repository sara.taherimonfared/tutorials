This directory provides all the necessary ingredients to build the Docker container for the CTEQ 2022 summer school in Pittsburgh. To build the container, simply run the following.
```
./build.sh
```
This can then be pushed to the Pythia Docker respoitory with the following.
```
docker push pythia8/tutorials:cteq22
```

There are a few points worth noting here.
* The working Pythia version for this tutorial is 8.307.
* The worksheet has been converted to Python, and is provided in `notebooks/worksheet8307.ipynb`. The full interface is built for this tutorial, e.g. `./generate --full`.
* The `start.sh` script is run from within the Docker container to set up the user environment, and is run by default from within the container. The script copies all the Pythia documentation and examples to a local `tutorial` directory. It then start the Jupyter server.
* The Python `stdout` is different from the C++ `stdout` which can cause some issues with the way the Jupyter notebook picks up output. The `wurlitzer` package is used to fix this issue, and is added to the startup sequence of IPython.
