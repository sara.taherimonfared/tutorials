# PYTHIA configuration file.
# Generated on Ke 10 Maa 2021 09:31:20 EET with the user supplied options:
# --with-rivet=/Users/ilkkah/work/Libraries/rivet
# --with-hepmc2=/Users/ilkkah/work/Libraries/hepMC2

# Install directory prefixes.
PREFIX_BIN=/Users/ilkkah/work/workspace/pythia/pythia8303/bin
PREFIX_INCLUDE=/Users/ilkkah/work/workspace/pythia/pythia8303/include
PREFIX_LIB=/Users/ilkkah/work/workspace/pythia/pythia8303/lib
PREFIX_SHARE=/Users/ilkkah/work/workspace/pythia/pythia8303/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
CXX=g++
CXX_COMMON=-O2 -std=c++11 -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-dynamiclib
CXX_SONAME=-Wl,-dylib_install_name,@rpath/
LIB_SUFFIX=.dylib
OBJ_COMMON=

EVTGEN_USE=false
EVTGEN_CONFIG=
EVTGEN_BIN=
EVTGEN_INCLUDE=
EVTGEN_LIB=

FASTJET3_USE=true
FASTJET3_CONFIG=fastjet-config
FASTJET3_BIN=
FASTJET3_INCLUDE=-I/Users/ilkkah/work/Libraries/fastjet/include
FASTJET3_LIB=-L/Users/ilkkah/work/Libraries/fastjet/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/fastjet/lib -lfastjet

HEPMC2_USE=true
HEPMC2_CONFIG=
HEPMC2_BIN=/Users/ilkkah/work/Libraries/hepMC2/
HEPMC2_INCLUDE=-I/Users/ilkkah/work/Libraries/hepMC2/include
HEPMC2_LIB=-L/Users/ilkkah/work/Libraries/hepMC2/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/hepMC2/lib -lHepMC

HEPMC3_USE=false
HEPMC3_CONFIG=
HEPMC3_BIN=
HEPMC3_INCLUDE=
HEPMC3_LIB=

LHAPDF5_USE=false
LHAPDF5_CONFIG=
LHAPDF5_BIN=
LHAPDF5_INCLUDE=
LHAPDF5_LIB=

LHAPDF6_USE=false
LHAPDF6_CONFIG=
LHAPDF6_BIN=
LHAPDF6_INCLUDE=
LHAPDF6_LIB=

POWHEG_USE=false
POWHEG_CONFIG=
POWHEG_BIN=
POWHEG_INCLUDE=
POWHEG_LIB=

RIVET_USE=true
RIVET_CONFIG=rivet-config
RIVET_BIN=/Users/ilkkah/work/Libraries/rivet/bin/
RIVET_INCLUDE=-I/Users/ilkkah/work/Libraries/rivet/include -I/Users/ilkkah/work/Libraries/fastjet/include -I/Users/ilkkah/work/Libraries/hepMC2/include -I/Users/ilkkah/work/Libraries/yoda/include
RIVET_LIB=-L/Users/ilkkah/work/Libraries/rivet/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/rivet/lib -lRivet -L/Users/ilkkah/work/Libraries/fastjet/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/fastjet/lib -lfastjet -L/Users/ilkkah/work/Libraries/hepMC2/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/hepMC2/lib -lHepMC -L/Users/ilkkah/work/Libraries/yoda/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/yoda/lib -lYODA

ROOT_USE=false
ROOT_CONFIG=rivet-config
ROOT_BIN=
ROOT_INCLUDE=
ROOT_LIB=

YODA_USE=true
YODA_CONFIG=yoda-config
YODA_BIN=
YODA_INCLUDE=-I/Users/ilkkah/work/Libraries/yoda/include
YODA_LIB=-L/Users/ilkkah/work/Libraries/yoda/lib -Wl,-rpath,/Users/ilkkah/work/Libraries/yoda/lib -lYODA

GZIP_USE=false
GZIP_CONFIG=yoda-config
GZIP_BIN=
GZIP_INCLUDE=
GZIP_LIB=

PYTHON_USE=false
PYTHON_CONFIG=yoda-config
PYTHON_BIN=
PYTHON_INCLUDE=
PYTHON_LIB=

MG5MES_USE=false
MG5MES_CONFIG=yoda-config
MG5MES_BIN=
MG5MES_INCLUDE=
MG5MES_LIB=

OPENMP_USE=false
OPENMP_CONFIG=yoda-config
OPENMP_BIN=
OPENMP_INCLUDE=
OPENMP_LIB=
