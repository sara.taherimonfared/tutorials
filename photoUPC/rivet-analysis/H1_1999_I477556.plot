# BEGIN PLOT /H1_1999_I477556/d01-x01-y01
XLabel=$p_{\mathrm{T}}$ [GeV/c]
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}\eta\mathrm{d}p^2_{\mathrm{T}}$ [nb]
RatioPlotSameStyle=1
YLabelSep=5.5
RatioPlotYMin=0.0
RatioPlotYMax=1.7
#GofLegend=1
# END PLOT

# BEGIN PLOT /H1_1999_I477556/d02-x01-y01
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [nb]
CustomLegend=$ p_{\mathrm{T}} > 2.0$ GeV/c
LogY=0
YLabelSep=5.8
RatioPlotSameStyle=1
YMin=0.0
YMax=3000.0
RatioPlotYMin=0.0
RatioPlotYMax=1.7
LegendXPos=0.05
LegendYPos=0.95
#RatioPlotReference=/merged.yoda/H1_1999_I477556/d02-x01-y01
#RatioPlotYLabel=ratio to total
#RatioPlotMode=datamc
#GofLegend=1
# END PLOT

# BEGIN PLOT /H1_1999_I477556/d02-x01-y02
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [nb]
CustomLegend=$ p_{\mathrm{T}} > 3.0$ GeV/c
LogY=0
YLabelSep=5.8
RatioPlotSameStyle=1
YMin=0.0
YMax=800.0
RatioPlotYMin=0.0
RatioPlotYMax=1.7
LegendXPos=0.05
LegendYPos=0.95
#GofLegend=1
# END PLOT
