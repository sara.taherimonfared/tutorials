// -*- C++ -*-
#include "Rivet/Analysis.hh"

#ifndef ENABLE_HEPMC_3
#include "HepMC/HepMCDefs.h"
#endif

namespace Rivet {

  /// @brief Analysis for the generated cross section
  class MC_XS_MCNET : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    MC_XS_MCNET()
      : Analysis("MC_XS_MCNET")
    {    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      /// @todo Convert to Scatter1D or Counter
      book(_h_XS, "XS");
      book(_h_N, "N");
      book(_h_pmXS, "pmXS");
      book(_h_pmN, "pmN");
      _mc_xs = _mc_error = 0.;
      sumWeightsPos = sumWeightsNeg = 0.;
      Nevents = Npos = Nneg = 0.;
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {
      //_h_N->fill(0.5);
      //_h_pmXS->fill(0.5);
      //_h_pmN ->fill(0.5);
      
      ++Nevents;
      if (event.weights()[0] > 0) {
        ++Npos;
	sumWeightsPos += event.weights()[0];
      } else {
        ++Nneg;
	sumWeightsNeg += event.weights()[0];
      }      

      #if defined ENABLE_HEPMC_3
      //@todo HepMC3::GenCrossSection methods aren't const accessible :(
      RivetHepMC::GenCrossSection gcs = *(event.genEvent()->cross_section());
      _mc_xs    = gcs.xsec();
      _mc_error = gcs.xsec_err();
      #elif defined HEPMC_HAS_CROSS_SECTION
      _mc_xs    = event.genEvent()->cross_section()->cross_section();
      _mc_error = event.genEvent()->cross_section()->cross_section_error();
      #endif // VERSION_CODE >= 3000000
      
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      //scale(_h_pmXS, crossSection()/sumOfWeights());
      #ifndef HEPMC_HAS_CROSS_SECTION
      _mc_xs = crossSection();
      _mc_error = 0.0;
      #endif
      _h_XS->addPoint(0, _mc_xs, 0.5, _mc_error);
      _h_N->addPoint(0, Nevents, 0.5, 0);
      _h_pmN->addPoint(0.5, Npos, 0.5, 0);
      _h_pmN->addPoint(-0.5, Nneg, 0.5, 0);
      double posXS = _mc_xs/sumOfWeights()*sumWeightsPos/sumOfWeights();
      double negXS = _mc_xs/sumOfWeights()*sumWeightsNeg/sumOfWeights();

      _h_pmXS->addPoint(0.5,posXS,0.5,0);
      _h_pmXS->addPoint(-0.5,negXS,0.5,0);
    }

    //@}


  private:

    /// @name Histograms
    //@{
    Scatter2DPtr _h_XS;
    Scatter2DPtr _h_N;
    Scatter2DPtr _h_pmXS;
    Scatter2DPtr _h_pmN;
    double _mc_xs, _mc_error;
    double sumWeightsPos, sumWeightsNeg;
    int Nevents,Npos,Nneg;
    //@}

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_XS_MCNET);

}
