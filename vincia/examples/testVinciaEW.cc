#include "Pythia8/Pythia.h"
#include "Pythia8/Vincia.h"
#include <stdlib.h> 
#include <sstream>

using namespace Pythia8;

int main() {
  double xMax = 1;
  double xMin = 1e-4;
  int nBins = 50;

  // A map of histograms
  map<int, Hist> spectra;
  spectra.emplace(make_pair(22,   Hist("22", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(11,   Hist("11", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(-11,  Hist("-11", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(2212, Hist("2212", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(-2212,Hist("-2212", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(12,   Hist("12", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(14,   Hist("14", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(16,   Hist("16", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(-12,  Hist("-12", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(-14,  Hist("-14", nBins, xMin, xMax, true)));
  spectra.emplace(make_pair(-16,  Hist("-16", nBins, xMin, xMax, true)));

  // Pythia objects
  Pythia pythia;
  Event& event = pythia.event;

  // Settings

  // As a stand-in for heavy DM decay, we use e+e- > nu nubar
  pythia.readString("Beams:idA  =  11");
  pythia.readString("Beams:idB  = -11");
  pythia.readString("WeakSingleBoson:ffbar2ffbar(s:gmZ) = on");
  pythia.readString("23:onMode  = off");
  pythia.readString("23:onIfAny = 12");
  pythia.readString("PartonLevel:MPI = off");

  // The mass of the DM particle
  double Ecm = 20000;
  pythia.readString("Beams:eCM = " + to_string(Ecm));

  // Don't do initial state radiation
  pythia.readString("PDF:lepton = off");

  // Turn on Pythia's weak shower
  pythia.readString("TimeShower:weakShower = on");
  // pythia.readString("PartonShowers:model = 2");
  // pythia.readString("Vincia:ewMode = 3");
  // pythia.readString("Vincia:mePlugin = procs_ew_sm-ckm");

  // Decay everything down to stable particles
  pythia.readString("13:mayDecay   = true");
  pythia.readString("211:mayDecay  = true");
  pythia.readString("321:mayDecay  = true");
  pythia.readString("130:mayDecay  = true");
  pythia.readString("2112:mayDecay = true");

  if(!pythia.init()) { return EXIT_FAILURE; }

  int nEvents = 1e5;

  for (int iEvent = 0; iEvent < nEvents; ++iEvent) {
    pythia.next();

    // Loop over all final state particles and bin their energy
    for (int i=0; i<event.size(); i++) {
      if (event[i].status() > 0) {
        int idNow = event[i].id();
        double x = 2*event[i].e()/Ecm;

        auto it = spectra.find(idNow);
        if (it != spectra.end()) {
         spectra[idNow].fill(x);
        }
        else {
          cout << "Error: Undecayed particle" << endl;
        }
      }
    }
  }

  // Output
  for (auto it=spectra.begin(); it!=spectra.end(); it++) {
    it->second.normalizeSpectrum(nEvents);
    string fileNameNow = "vinciaEWSpectra/pythia_" + it->second.getTitle() + ".csv";
    // string fileNameNow = "vinciaEWSpectra/vincia_" + it->second.getTitle() + ".csv";
    ofstream outf(fileNameNow);
    if (outf.is_open()) {
      it->second.pyplotTable(outf);
    }
    else {
      cout << "Failed to open file" << endl;
      exit(0);
    }
  }

  pythia.stat();

}