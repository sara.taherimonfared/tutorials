from matplotlib import pyplot as plt
import argparse
import os
import csv
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from HDMSpectra import HDMSpectra

def plot(file, label, color):
  data = np.genfromtxt(file, delimiter="  ")
  x = data[:-1,0]
  y = data[:-1,1]
  plt.plot(x, x*x*y, label=label, color=color)

# Type of neutrino and DM mass
id_nu = 12
mDM = 20000

# A list of all stable particles we get a spectrum for
id_dict = {
  22: r'\gamma', 
  2212: r'p',
  -2212: r'\bar{p}', 
  11: r'e', 
  12: r'\nu_{e}', 
  14: r'\nu_{\mu}',
  16: r'\nu_{\tau}',
  -11: r'\bar{e}',
  -12: r'\bar{\nu}_{e}',
  -14: r'\bar{\nu}_{\mu}',
  -16: r'\bar{\nu}_{\tau}'}

with PdfPages("plots.pdf") as pdf:
  # Loop through all stable ids
  for id_final in id_dict:

    plt.yscale('log')
    plt.xscale('log')

    # Plot the prediction from 2007.15001
    x_hdm = np.logspace(-4.,0.,1000)
    spec_hdm = HDMSpectra.spec(id_final, id_nu, x_hdm, mDM, "HDMSpectra.hdf5")
    plt.plot(x_hdm, x_hdm*x_hdm*spec_hdm, label="2007.15001", color='red')

    # Look through the files to find the ones for this id
    for filename in os.listdir("."):
      if filename.endswith(".csv") and (int(filename.split("_")[1].split(".")[0]) == id_final):
        if "pythia" in filename:
          plot(filename, "Pythia", 'green')
        if "vincia" in filename:
          plot(filename, "Vincia", 'blue')

    plt.xlabel("$x = 2E/m_{\chi}$")
    plt.ylabel("$x^2 dN/dx$")

    plt.title(r'$\chi \rightarrow \nu_{{e}} \bar{{\nu}}_{{e}} \rightarrow {{{}}}$'.format(id_dict[id_final]))

    plt.legend()
    pdf.savefig()
    plt.clf()